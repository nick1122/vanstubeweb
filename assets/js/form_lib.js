var headID = document.getElementsByTagName('head')[0];

var link = document.createElement('link');

link.type = 'text/css';
link.rel = 'stylesheet';
//link.href = '/assets/js/common/form/form.css';
headID.appendChild(link);

//document.write('<script type="text/javascript" src="/uav/assets/js/jquery.form.min.js"></script>');

$(function ($) {
    form_lib = function () {
        var form_lib = this;

        form_lib.form_id_tag = '';

        form_lib.do_submit = function (form_id, form_action, dataType) {

            dataType = (dataType == undefined) ? 'json' : dataType;

            if (form_action != undefined) {
                document.getElementById(form_id).action = form_action;
            }

            form_lib.form_id_tag = form_id;

            $("#" + form_id).ajaxSubmit({
                beforeSubmit: form_lib.before_submit,
                timeout: 30000,
                //error: form_lib.submit_error,
                error: showError,
                success: form_lib.submit_success,
                dataType: dataType
            });
        }

        form_lib.before_submit = function () {
            $('.form_err').hide().html('');
        }

        form_lib.submit_error = function () {
            $('.form_err').html();
        }
        function showError(xhr, ajaxOptions, thrownError) {
            //showResponse(responseText, statusText, xhr, $form);
            $('div#errorMsg').text("responseText: " + xhr.responseText
                + ", textStatus: " + xhr.statusText
                + ", errorThrown: " + thrownError);
            $('div#error , form#formData').addClass("error");
        }

        form_lib.alert_error = function (text) {
            //$('#common_alert span').html(text);
            //$('#common_alert').removeClass().addClass('alert alert-danger').show();
            //form_lib.position($('#message_container'));

            alert(text);
            //$('#common_alert span').html(text);
            //$('#common_alert').removeClass().addClass('alert alert-danger').show();
            //$('#message_container').show();
            //form_lib.position($('#common_alert'));
        }

        form_lib.alert_success = function (text) {
            $('#common_alert span').html(text);
            $('#common_alert').removeClass().addClass('alert alert-success').show();
            $('#message_container').show();
            form_lib.position($('#common_alert'));
        }

        //form_lib.position = function (elem) {
        //
        //    $('html').scrollTop((elem.offset().top) - 50);
        //}

        form_lib.show_form_error = function (data, type) {

            if (data['text']['common'] != undefined) {
                form_lib.alert_error(data['text']['common']);
                return;
            } else if (data['text'] != undefined) {

                if (!type) {
                    form_lib.alert_error(data['text']);
                    var error_text = '';
                    for (var key in data.text) {
                        for (var i = 0; i < data.text[key].length; i++) {
                            error_text = error_text + '<div>' + data.text[key][i] + '</div>';
                        }
                    }
                    form_lib.alert_error(error_text);
                } else {
                    /* 先each是否含有common，若有則直接顯示在common區，其餘錯誤不顯示 */
                    //array('common' => 'xxxxxxxxxxx) 
                    //array('text' => array('common' => 'xxxxxxxxxx'))
                    //以上兩種皆需判斷
                    /* END 先each是否含有common，若有則直接顯示在common區，其餘錯誤不顯示 */
                    var show_position = '';

                    for (var key in data.text) {
                        for (var i = 0; i < data.text[key].length; i++) {
                            /* 緊臨時 */

                            //$('#' + form_lib.form_id_tag + ' *[name="contract_number"]').next('.form_err').append('<p>' + data.text[key][i] + '</p>').show();

                            if ($("#" + form_lib.form_id_tag + ' #' + key).length) {

                                if (!show_position) {
                                    show_position = $("#" + form_lib.form_id_tag + ' #' + key);
                                }

                                $("#" + form_lib.form_id_tag + ' #' + key).next('.form_err').html('<p>' + data.text[key][i] + '</p>').show();

                                $("#" + form_lib.form_id_tag + ' #' + key).nextAll('.form_err:first').html(data.text[key][i]).show();
                            } else {
                                if (!show_position) {
                                    show_position = $("#" + form_lib.form_id_tag + ' *[name="' + key + '"]');
                                }

                                if ($("#" + key + "_err")) {
                                    show_position = "";
                                    $("#" + key + "_err").html('<p>' + data.text[key][i] + '</p>').show();
                                    continue;
                                }

                                $("#" + form_lib.form_id_tag + ' *[name="' + key + '"]').next('.form_err').html('<p>' + data.text[key][i] + '</p>').show();

                                $("#" + form_lib.form_id_tag + ' *[name=' + key + ']').nextAll('.form_err:first').html(data.text[key][i]).show();
                            }


                            /* END 緊臨時 */

                            /* 包含在元素內時 */
//                            $("#" + form_lib.form_id_tag + ' *[name=' + data.text[i].name + ']').nextAll().find('.form_err').append(data.text[i].message).show();
                            /* END　包含在元素內時 */

                            /*　有元素間隔時 */
//                            $("#" + form_lib.form_id_tag + ' *[name=' + data.text[i].name + ']').nextAll('.form_err:first').append(data.text[i].message).show();
                            /*　END 有元素間隔時 */
                        }
                    }
                    if (show_position) {
                        form_lib.position(show_position);

                    }
                }
            }
        }

        form_lib.submit_success = function (data, status) {
            if (data.code !== 0) {
                form_lib.show_form_error(data);

            } else {
                form_lib.show_success(data);
            }
        }

        form_lib.show_success = function (data) {
            if (data.view != undefined) {
                $('#global_container').html(data.view);
            }
        }
    }

});