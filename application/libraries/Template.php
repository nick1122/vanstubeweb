<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/5/20
 * Time: 上午 05:52
 */
class Template
{
    private $_css_path = array();
    private $_js_path = array();

    function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->session_id = session_id();

    }
    public function showview($path, $data = null, $return = false)
    {

        $html = $this->ci->load->view($path, $data, true);
        if ($return) {
            return $html;
        }

        $show = array(
            'tmpl_data' => $html
        );

        $this->ci->load->view('template', $show);
    }
}