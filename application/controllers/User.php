<?php

class User extends MY_Controller
{

    public function register()
    {
        $this->template->showview('register');
    }

    public function login()
    {
        if (isset($_SESSION["user"]) && $_SESSION["user"] != NULL) {
            redirect(site_url("/"));
            return true;
        }
        $this->template->showview("login", Array("pageTitle" => "Login"));

    }

    public function logining()
    {
        if (isset($_SESSION["user"]) && $_SESSION["user"] != NULL) {
            redirect(site_url("/"));
            return true;
        }
        $account = $this->input->post("account");
        $password = $this->input->post("password");

        $this->load->model("User_Model");
        $user = $this->User_Model->getUser($account, $password);
        if ($user == null) {
            $this->load->view("login", Array("pageTitle" => "Login",
                "account" => $account, "errorMessage" => "Account/Password Error!!!!!!!".$account."//".$password));
        }else{
        $_SESSION["user"] = $user;
        redirect(site_url("/"));

    }}

    public function logout()
    {
        session_destroy();
        redirect(site_url("/user/login"));
    }

    public function registering()
    {

        $account = $this->input->post("account");
        $password = $this->input->post("password");
        $passwordrt = $this->input->post("passwordrt");
        if (trim($password) == "" || trim($account) == "") {
            $this->template->showview('register', Array(
                "errorMessage" => "Account/Password shouldn't be empty", "account" => $account));
            return false;


        }

        //.
        if ($password != $passwordrt) {
            $this->template->showview('register', Array(
                "errorMessage" => "Passoword doesn't match",
                "account" => $account
            ));
            return false;
        }

        $this->load->model("User_Model");
        //check account exit
        if ($this->User_Model->checkUserExist(trim($account))) {
            $this->template->showview('register', Array(
                "errorMessage" => "This account is already in used!!",
                "account" => $account
            ));
            return false;
        }
        $this->User_Model->insert(trim($account), trim($password)); //完成新增動作
        $this->template->showview('register_success', Array(
            "account" => $account));
    }
}

?>