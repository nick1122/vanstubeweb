<?php if (!defined('BASEPATH')) exit ('No direct script access allowed ');

class Article extends MY_Controller
{

    public function index()
        /*
        @hotArticles->Views 點閱次數
        @hotArticles->Title 標題
        @hotArticles->Account 作者



        */
    {
        $this->load->model("Article_Model");
        $hotArticles = $this->Article_Model->getHotArticles(100);

        $this->template->showview('welcome_message',
            Array("pageTitle" => "首頁",
                "hotArticles" => $hotArticles
            )
        );


    }


    public function post()
    {
        if (!isset($_SESSION["user"])) {
            redirect(site_url("/user/login"));
            return true;
        }
        $this->template->showview('article_post', Array(
            "pageTitle" => "Article Post"
        ));
    }

    public function posting()
    {
        if (!isset($_SESSION["user"])) {
            redirect(site_url("/user/login"));
            return true;
        }
        $title = trim($this->input->post("title"));
        $content = trim($this->input->post("content"));
        if ($title == "" || $content == "") {
            $this->load->view('article_post', Array(
                "pageTitle" => "Article Post",
                "errorMessage" => "Title/Content shouldn't be empty!!!",
                "title" => $title,
                "content" => $content
            ));
            return false;
        }
        $this->load->model("Article_Model");
        $insertID = $this->Article_Model->insert($_SESSION["user"]->UserID, $title, $content);
        redirect(site_url("article/postSuccess/" . $insertID));

    }

    public function postSuccess($articleID)
    {
        $this->template->showview('article_success', Array(
            "pageTitle" => "Success",
            "articleID" => $articleID
        ));
    }

    public function view($articleID = null)
    {
        if ($articleID == null) {
            show_404("Article not found!");
            return true;

        }
        $this->load->model("Article_Model");
        $article = $this->Article_Model->get($articleID);

        if ($article == null) {
            show_404("Article not found !");
            return true;
        }
        //add article count
        $this->Article_Model->updateViews($articleID, $article->Views + 1);
        $this->template->showview('article_view', Array(
            "pageTitle" => "Post Article",
            "article" => $article
        ));

    }


    public function author($author = null, $offset = 0)
    {
        if ($author == null) {
            show_404("Author not found!");
            return true;
        }
        $this->load->model("User_Model");
        $this->load->model("Article_Model");
        $user = $this->User_Model->getUserByAccount($author);
        if ($user == null) {
            show_404("Author not found!");
        }
        $pageSize = 5;
        $this->load->library('pagination');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('/article/author/' . $author . '/');
        #get total count
        $config['total_rows'] = $this->Article_Model->countArticlesByUserId($user->UserID);
        $config['per_page'] = $pageSize;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $results = $this->Article_Model->getArticlesByUserID($user->UserID, $offset, $pageSize);
        $this->template->showview('article_author',
            Array(
                "pageTitle" => $user->Account . "'s posts",
                "results" => $results,
                "user" => $user,
                "pageLinks" => $this->pagination->create_links()
            ));
    }

    public function edit($articleID = null)
    {
        if (!isset($_SESSION["user"]) || $_SESSION["user"] == null) {
            redirect(site_url("/user/login"));
            return true;
        }
        if ($articleID == null) {
            show_404("Article not found!");
            return true;
        }
        $this->load->model("Article_Model");
        $article = $this->Article_Model->get($articleID);

        if ($article->Author != $_SESSION["user"]->UserID) {
            show_404("Article not found!");
            redirect(site_url("/"));
            return true;
        }
        $this->template->showview('article_edit', Array(
            "pageTitle" => "Edit Article[" . $article->Title . "]",
            "article" => $article
        ));

    }

    public function update()
    {
        $articleID = $this->input->post("articleID");

        if (!isset($_SESSION["user"]) || $_SESSION["user"] == null) {
            redirect(site_url("/user/login"));
            return true;

        }
        if ($articleID == null) {
            show_404("Article not found!");
            return true;
        }
        $this->load->model("Article_Model");
        $article = $this->Article_Model->get($articleID);

        if ($article->Author != $_SESSION["user"]->UserID) {
            show_404("Article not Found~");
            redirect(site_url("/"));
            return true;
        }
        $this->Article_Model->updateArticle(
            $articleID,
            $this->input->post('title'),
            $this->input->post("content")
        );
        redirect(site_url("article/view/" . $articleID));

    }

    public function del($articleID = null)
    {
//就算是進行更新動作，該做的檢查還是都不能少
        if (!isset($_SESSION["user"]) || $_SESSION["user"] == null) {
//沒有登入的，直接送他去登入。
            redirect(site_url("/user/login"));
            return true;
        }

        if ($articleID == null) {
            show_404("Article not found !");
            return true;
        }

        $this->load->model("Article_Model");
//完成取資料動作
        $article = $this->Article_Model->get($articleID);

        if ($article->Author != $_SESSION["user"]->UserID) {
            show_404("Article not found !");
//不是作者又想編輯，顯然是來亂的，送他回首頁。
            redirect(site_url("/"));
            return true;
        }

        $this->Article_Model->del(
            $articleID
        );

//更新完後送他回個人文章頁面
        redirect(site_url("article/author/" . $_SESSION["user"]->Account));
    }

}


?>