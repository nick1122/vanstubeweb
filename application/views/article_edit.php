<script type="text/javascript" src="<?= base_url("/assets/js/tinymce/js/tinymce/tinymce.min.js") ?>"></script>
<div class="content">
    <form action="<?= site_url("/article/update") ?>" method="post">
        <input type="hidden" name="articleID" value="<?= $article->ArticleID ?>"/>
        <?php if (isset($errorMessage)) { ?>
            <div class="alert-error"><?= $errorMessage ?></div>
        <?php } ?>
        <table>
            <tr>
                <td>標題</td>
                <td><input class="form-control" type="text" name="title"
                           value="<?= htmlspecialchars($article->Title) ?>"/>
                </td>
            </tr>
            <tr>
                <td> 內容</td>
                <td><input class="form-control" name="content" rows="10" cols="60" value="<?php
                    echo htmlspecialchars($article->Content);
                    ?>"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <a class="btn btn-success" href="<?= site_url("/") ?>">取消</a>
                    <input type="submit" class="btn" value="送出"/>
                </td>
            </tr>
        </table>
    </form>
</div>