<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta property="og:url" content="http://www.vanstube.com"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="VansTube"/>
    <meta property="og:description" content="VansTube"/>
    <meta property="og:image" content="https://developers.facebook.com/images/devsite/fb4d_logo-2x.png"/>
    <title></title>
    <!--<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">-->
    <link rel="stylesheet" href="<?= base_url("/assets/css/bootstrap.theme.css") ?>">
    <link rel="stylesheet" href="<?= base_url("/assets/css/bootstrap-responsive.min.css") ?>">
    <link rel="stylesheet" href="<?= base_url("/assets/css/component.css") ?>">
    <link rel="stylesheet" href="<?= base_url("/assets/css/main.css") ?>">
    <!-- google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900%7CMerriweather:400,400italic,300,300italic,700,700italic' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?= base_url("assets/bootstrap-3.3.4-dist/js/bootstrap.min.js") ?>"></script>
</head>
<body>

<div class="content-header">
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= site_url("/") ?>">VansTube娛樂</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?= site_url("/") ?>">Home</a></li>
                <?php if (isset($_SESSION["user"]) && $_SESSION["user"] != null): ?>
                    <li><a href="<?= site_url("article/author/" . $_SESSION["user"]->Account) ?>">My Articles</a></li>
                <?php endif; ?>
            </ul>
            <!-- login status -->
            <?php if (isset($_SESSION["user"]) && $_SESSION["user"] != null): ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Hi <?= $_SESSION["user"]->Account ?></a></li>
                    <li class="divider-vertical"></li>
                    <li><a href="<?= site_url("article/post") ?>">發文</a></li>
                    <li><a href="<?= site_url("user/logout") ?>">登出</a></li>
                </ul>
            <?php endif; ?>
            <?php if (!isset($_SESSION["user"])): ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="javascript: void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('http://vanstube.com/'))));">點我分享</a></li>
                    <li><a href="<?= site_url("user/login") ?>">登入</a></li>
                    <li class="divider-vertical"></li>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="container">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- 橫 -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:100%;height:90px"
         data-ad-client="ca-pub-3467214901034310"
         data-ad-slot="5615201385"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>

<div class="container">
    <div class="col-sm-2">
        <ul class="list-group">
            <li class="list-group-item">
                <span class="badge">14</span>
                分類一
            </li>

        </ul>
    </div>
    <div class="col-sm-10">
        <?= $tmpl_data ?>
    </div>
</div>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


</body>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-80924027-1', 'auto');
    ga('send', 'pageview');

</script>
</html>