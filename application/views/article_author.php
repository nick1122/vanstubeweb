<div class="content">
    <h1><?= $user->Account ?></h1>
    <?php foreach ($results as $article) { ?>
        <table class="table table-bordered">
            <tr  class="active">
                <td>
                    <a href="<?= site_url("article/view/" . $article->ArticleID) ?>"><?= htmlspecialchars($article->Title) ?></a>
                </td>
            </tr>
            <tr>
                <td><?= nl2br(htmlspecialchars($article->Content)) ?></td>
            </tr>
        </table>
    <?php } ?>
    <p>
        <?= $pageLinks ?>
    </p>
</div>
</div>
