<div role="main">
    <ul class="tiles-wrap animated" id="wookmark1">
        <!-- These are our grid blocks -->
        <?php foreach ($hotArticles as $key => $article): ?>
            <a href="<?= site_url("article/view/" . $article->ArticleID) ?>">

                <li>
                    <img class="img-responsive" style="width: 100%;" src="http://i.ytimg.com/vi/<?= $article->Content ?>/0.jpg">
                    <h3><?= htmlspecialchars($article->Title) ?></h3>
                    <span class="viewcount"><i class="fa fa-eye" aria-hidden="true"></i><?= htmlspecialchars($article->Views) ?></span>
                </li>
            </a>
        <?php endforeach; ?>
    </ul>
</div>

<script src="<?= base_url("/assets/js/wookmark.js") ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var wookmark1 = new Wookmark('#wookmark1', {
            outerOffset: 10, // Optional, the distance to the containers border
            itemWidth: 210 // Optional, the width of a grid item
        });
    });
    </script>
