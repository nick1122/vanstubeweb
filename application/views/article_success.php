<div class="container">
    <div class="content">
        <div class="alert alert-success">
            Success,<a href="<?= site_url("article/view/" . htmlspecialchars($articleID)) ?>">Go!</a>
        </div>
    </div>
</div>
