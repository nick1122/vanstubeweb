<script src='https://www.google.com/recaptcha/api.js'></script>
<?php
$this->load->helper('captcha');
$phase1 = $this->input->post("aaa");
if ($phase1 != 1) {
    ?>
    <div class="container article-view">
        <div class="col-lg-8 col-lg-offset-2">
            <form action="" method="post">
                <input type="hidden" name="aaa" value="1"><br><br>
                <div class="g-recaptcha" data-sitekey="6LdibCUTAAAAANmQdGwsZ6shy9CWw_Y9kRXBAiJV"></div>
                <input class="btn btn-success" type="submit" value="Submit">
            </form>
        </div>
    </div>
    <?php
}
if ($phase1 == 1) {
    ?>
    <script src="<?= base_url("assets/js/Magnific-Popup-master/dist/jquery.magnific-popup.min.js") ?>"></script>
    <link rel="stylesheet" href="<?= base_url("assets/js/Magnific-Popup-master/dist/magnific-popup.css") ?>">
    <div class="container article-view">
        <div class="col-lg-8 col-lg-offset-2">
            <table class="table table-bordered">
                <tr>
                    <td><?= htmlspecialchars($article->Title) ?></td>
                </tr>
                <tr>
                    <td>
                        <img class="popup-youtube" style="width: 100%;" src="http://i.ytimg.com/vi/<?= nl2br(htmlspecialchars($article->Content)) ?>/0.jpg"
                             href="http://www.youtube.com/watch?v=<?= nl2br(htmlspecialchars($article->Content)) ?>">
                    </td>
                </tr>
                <?php if (isset($_SESSION["user"]) && $_SESSION["user"] != null
                    && $_SESSION["user"]->UserID == $article->Author
                ): ?>
                    <tr>
                        <td colspan="2">
                            <a class="btn btn-success" href="<?= site_url("article/edit/" . $article->ArticleID) ?>">編輯此文章</a>
                            <a class="btn btn-success" href="<?= site_url("article/edit/" . $article->ArticleID) ?>">編輯此文章</a>
                        </td
                    </tr>
                <?php endif; ?>
                <tr>
                    <td>
                        <a class="btn btn-success" href="<?= base_url() ?>">回首頁</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });
        });
    </script>
<?php } ?>