<?php if (isset($errorMessage)) { ?>
    <div class="alert alert-error">
        <?= $errorMessage ?>
    </div>
<?php } ?>
<div class="container">
    <form action="<?= site_url("/user/logining") ?>" method="post">
        <div class="form-group">
            <label for="inputEmail" class="col-lg-2 control-label">Account</label>
            <div class="col-lg-4">
                <?php if (isset($account)) { ?>
                    <input class="form-control" type="text" name="account"
                           value="<?= htmlspecialchars($account) ?>"/>
                <?php } else { ?>
                    <input class="form-control" type="text" name="account"/>
                <?php } ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="inputEmail" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-4">
                <input class="form-control" type="password" name="password"/>
            </div>
        </div>
        <div class="col-lg-5 col-lg-offset-2 ">
            <input class="btn btn-primary" type="submit" value="送出"/>
        </div>

    </form>
</div>
