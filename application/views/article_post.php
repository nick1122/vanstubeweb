<script type="text/javascript" src="<?= base_url("/assets/js/tinymce/js/tinymce/tinymce.min.js") ?>"></script>
<div class="container post">
    <div class="content">
        <fieldset>
            <form action="<?= site_url("article/posting") ?>" method="post" class="form-horizontal">
                <?php if (isset($errorMessage)): ?>
                    <div class="alert alert-error"><?= $errorMessage ?></div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label">標題</label>
                    <div class="col-lg-4">
                        <?php if (isset($title)): ?>
                            <input class="form-control" type="text" name="title" value="<?= htmlspecialchars($title) ?>"/>
                        <?php endif; ?>
                        <?php if (!isset($title)): ?>
                            <input class="form-control" type="text" name="title"/>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label">影片</label>
                    <div class="col-lg-4">
                        <!--<input class="form-control" name="content" rows="10" cols="60" value="<?php
                      /*if (isset($content)) {
                            echo $content;
                        }
                        */?>"/>--></div>
                </div>
                <div class="from-group">
                <label for="inputEmail" class="col-lg-2 control-label">內容</label>
                <div>
                    <textarea class="form-control" name="content" style="width:500px;height:200px"> </textarea>
                </div>

                </div>

                <div class="col-lg-8 pull-right">
                    <a class="btn btn-default" href="<?= site_url("/") ?>">取消</a>
                    <input type="submit" class="btn btn-success" value="送出"/>
                </div>
            </form>
        </fieldset>
    </div>
</div>
